from django.urls import path

from . import views
from .views import login

def trigger_error(request):
    division_by_zero = 1 / 0

urlpatterns = [
    path('', views.index, name='index'),
    path('sentry-debug/', trigger_error, name='sentry'),
    path('login', views.login, name='login'),
    path('user', views.user, name='user'),
    path('popular-boardgames', views.top_five_boardgames, name='popular-boardgames'),
    path('register', views.register, name='register'),
    path('reset-password', views.reset_password, name='reset_password'),
    path('reset-password/<str:token>', views.reset_password_token, name="reset_password_token"),
    path('public-wishlist', views.public_wishlist, name='public_wishlist'),
    path('wishlist', views.wishlist, name='wishlist'),
    path('wishlist/<int:wishlist_id>', views.wishlist_by_id, name="wishlist_by_id"),
    path('search', views.search, name='search'),
    path('share-wishlist/<int:wishlist_id>', views.share, name='share'),
    path('share-wishlist/<int:wishlist_id>/<int:user_id>', views.remove_user, name='remove_user'),
    path('wishlist/<int:wishlist_id>/<int:boardgame_id>', views.wishlist_move_boardgame, name='wishlist_move_boardgame')
]