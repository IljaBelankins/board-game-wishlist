
# Migrations
To create a new migration firstly run:
```
$env:DATABASE_PASSWORD = 'password'
python manage.py makemigrations BoardGameWishlistApp
```

To run a migration:
```
python manage.py migrate
```

# Data Migrations
To run a data migration run:
```
$env:DATABASE_PASSWORD = 'password'
python manage.py makemigrations --empty BoardGameWishlistApp
```
and modify the migration file with SQL in operations list. Then run migrations

# Running
To run the app, first start up a database with
```
docker-compose -f docker-compose.yaml up
```

In another terminal in BoardGameWishlist, run the following:
```
$env:DATABASE_PASSWORD = 'docker'
$env:EMAIL_PASSWORD = 'password'
python manage.py runserver
```

# To deploy
To deploy, create PR from master to production. Merge,
wait for pipeline to run.
