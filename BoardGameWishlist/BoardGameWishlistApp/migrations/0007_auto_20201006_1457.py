# Generated by Django 3.1.1 on 2020-10-06 13:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('BoardGameWishlistApp', '0006_auto_20201006_1455'),
    ]

    operations = [
        migrations.AlterField(
            model_name='boardgame',
            name='name',
            field=models.CharField(max_length=100),
        ),
    ]
