from django.test import TestCase
from BoardGameWishlistApp.models import Wishlist, User, WishlistSharedUsers, ShareStatus

class WishlistTestCase(TestCase):
    def setup(self):
        self.owner_user = User.create_user(username="TestA", email="test@test.com", password="TestA")
        self.share_user = User.create_user(username="TestB", email="testB@test.com", password="TestB")
        self.write_user = User.create_user(username="TestC", email="testC@test.com", password="TestC")
        self.wishlist = Wishlist.create_wishlist(name="Test Wishlist", user=self.owner_user, visibility_status="PUBLIC")
        share_status = ShareStatus.get_share_status("READ")
        WishlistSharedUsers.share_wishlist_with_user(self.wishlist, self.share_user, share_status)
        share_status = ShareStatus.get_share_status("WRITE")
        WishlistSharedUsers.share_wishlist_with_user(self.wishlist, self.write_user, share_status)

    def test_owner_status_read(self):
        self.setup()
        result = self.wishlist.to_dict(self.share_user.pk)

        assert result["user_status"] == "READ"

    def test_owner_status_write(self):
        self.setup()
        result = self.wishlist.to_dict(self.write_user.pk)

        assert result["user_status"] == "WRITE"

    def test_owner_status_owner(self):
        self.setup()
        result = self.wishlist.to_dict(self.owner_user.pk)

        assert result["user_status"] == "OWNER"