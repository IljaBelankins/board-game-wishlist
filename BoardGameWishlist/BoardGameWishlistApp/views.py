from django.http import JsonResponse
from django.shortcuts import render
from django.db import connection
from rest_framework.request import Request
from rest_framework.decorators import parser_classes, api_view
from rest_framework.parsers import JSONParser
from rest_framework import status
from rest_framework.response import Response
from .models import User, Token, Wishlist, BoardGame, WishlistBoardGame, ShareStatus, WishlistSharedUsers, EmailResetToken
from rest_framework.exceptions import MethodNotAllowed, ParseError, NotAuthenticated, NotFound
from django.views.decorators.csrf import csrf_exempt
from django.core.mail import send_mail

def index(request):
    return render(request, 'home.html')

@api_view(["GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
def login(request: Request):
    if request.method == 'GET':
        username = request.query_params.get("username")
        password = request.query_params.get("password")
        if username is None or password is None:
            raise ParseError(detail="Username and Password must be provided.")
        try:
            user = User.login(username, password)
        except ValueError:
            raise ParseError(detail="Invalid login.")
        token = Token.create_token(user)
        data = {"token" : token.id}

        return JsonResponse(data, safe=False)
    else:
        raise MethodNotAllowed(request.method, detail=None, code=None)

@api_view(["POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
def register(request: Request):
    if request.method == 'POST':
        username = request.data.get("username")
        password = request.data.get("password")
        email = request.data.get("email")
        if username is None or password is None or email is None:
            raise ParseError(detail="Username, Password and Email must be provided.")
        try:
            user = User.create_user(username, email, password)
        except ValueError:
            raise ParseError(detail="Username and Email must be unique.")
        token = Token.create_token(user)
        data = {"token" : token.id}

        return JsonResponse(data, safe=False)
    else:
        raise MethodNotAllowed(request.method, detail=None, code=None)

@api_view(["POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
def reset_password(request: Request):
    if request.method == 'POST':
        username = request.data.get("username")
        email = request.data.get("email")
        if username is None or email is None:
            raise ParseError(detail="Username and Email must be provided.")
        try:
            user = User.get_user_by_username_and_email(username=username, email=email)
        except ValueError:
            return Response(status=status.HTTP_200_OK)
        email_reset_token = EmailResetToken.create_email_reset_token(user)

        send_mail(
            'Board Game Wishlist Password Reset',
            f"Click here to reset your password: http://bg-wishlist.iljabelankins.co.uk/reset-password/{email_reset_token.id}",
            'emailsystemsiljabelankins@gmail.com',
            [user.email],
            fail_silently=False
        )

        return Response(status=status.HTTP_200_OK)
    else:
        raise MethodNotAllowed(request.method, detail=None, code=None)

@api_view(["POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
def reset_password_token(request: Request, token: str):
    if request.method == 'POST':
        password = request.data.get("password")
        if password is None:
            raise ParseError(detail="Password must be provided.")
        try:
            user = EmailResetToken.get_user_from_token(token)
        except ValueError:
            raise NotAuthenticated(detail="Invalid token validation.")
        user.update_password(password)
        EmailResetToken.delete_token_by_id(token)


        return Response(status=status.HTTP_204_NO_CONTENT)
    else:
        raise MethodNotAllowed(request.method, detail=None, code=None)

@api_view(["POST", "GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
def wishlist(request: Request):
    if request.method == 'POST':
        headers = request.META
        token = headers.get("HTTP_TOKEN")
        if token is None:
            raise NotAuthenticated(detail="Token must be provided.")

        name = request.data.get("name")
        visibility = request.data.get("visibility")
        if name is None or visibility is None:
            raise ParseError(detail="Name and Visibility must be provided.")

        try:
            user = Token.validate_token(token)
        except ValueError:
            raise NotAuthenticated(detail="Token is invalid.")

        try: 
            wishlist = Wishlist.create_wishlist(user, name, visibility)
        except ValueError:
            raise ParseError(detail="Invalid status provided.")

        return JsonResponse(wishlist.to_dict(), safe=False)

    elif request.method == 'GET':
        headers = request.META
        token = headers.get("HTTP_TOKEN")
        if token is None:
            raise NotAuthenticated(detail="Token must be provided.")

        try:
            user = Token.validate_token(token)
        except ValueError:
            raise NotAuthenticated(detail="Token is invalid.")
        wishlists = user.get_viewable_lists()
        wishlists_dicts = []
        for wishlist in wishlists:
            wishlists_dicts.append(wishlist.to_dict(user.pk))

        return JsonResponse(wishlists_dicts, safe=False)
    else:
        raise MethodNotAllowed(request.method, detail=None, code=None)

@api_view(["PUT", "GET", "DELETE"])
@parser_classes((JSONParser, ))
@csrf_exempt
def wishlist_by_id(request: Request, wishlist_id):
    if request.method == 'PUT':
        headers = request.META
        token = headers.get("HTTP_TOKEN")
        if token is None:
            raise NotAuthenticated(detail="Token must be provided.")
        name = request.data.get("name")
        visibility_status = request.data.get("visibility")
        if name is None or visibility_status is None:
            raise ParseError(detail="Name or Visibility must be provided.")
        try:
            user = Token.validate_token(token)
        except ValueError:
            raise NotAuthenticated(detail="Token is invalid.")
        wishlist: Wishlist = user.get_editable_wishlist_by_id(wishlist_id)
        if not wishlist:
            raise NotFound(detail=f"Wishlist not found with ID. (wishlist id={wishlist_id})")
        wishlist.update_wishlist(name, visibility_status)

        return JsonResponse(wishlist.to_dict(user.pk), safe=False)


    elif request.method == 'GET':
        headers = request.META
        token = headers.get("HTTP_TOKEN")
        if token is None:
            wishlist = Wishlist.get_public_wishlist(wishlist_id)
            if not wishlist:
                raise NotFound(detail=f"Wishlist not found with ID. (wishlist id={wishlist_id})")
            return JsonResponse(wishlist.to_dict(override_user_status="READ"), safe=False)

        try:
            user = Token.validate_token(token)
        except ValueError:
            raise NotAuthenticated(detail="Token is invalid.")
        wishlist = user.get_wishlist_by_id(wishlist_id)
        if not wishlist:
            wishlist = Wishlist.get_public_wishlist(wishlist_id)
            if not wishlist:
                raise NotFound(detail=f"Wishlist not found with ID. (wishlist id={wishlist_id})")
            return JsonResponse(wishlist.to_dict(override_user_status="READ"), safe=False)
        if not wishlist:
            raise NotFound(detail=f"Wishlist not found with ID. (wishlist id={wishlist_id})")
        return JsonResponse(wishlist.to_dict(user.pk), safe=False)


    elif request.method == 'DELETE':
        headers = request.META
        token = headers.get("HTTP_TOKEN")
        if token is None:
            raise NotAuthenticated(detail="Token must be provided.")
        try:
            user = Token.validate_token(token)
        except ValueError:
            raise NotAuthenticated(detail="Token is invalid.")
        wishlist = user.get_owned_wishlist_by_id(wishlist_id)
        wishlist.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)
    else:
        raise MethodNotAllowed(request.method, detail=None, code=None)

@api_view(["GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
def search(request: Request):
    if request.method == 'GET':
        headers = request.META
        token = headers.get("HTTP_TOKEN")
        if token is None:
            raise NotAuthenticated(detail="Token must be provided.")
        try:
            user = Token.validate_token(token)
        except ValueError:
            raise NotAuthenticated(detail="Token is invalid.")
        
        query = request.query_params.get("query")
        if query is None:
            raise ParseError(detail="Query must be provided.")
        boardgames = BoardGame.objects.filter(name__icontains=query)[0:50]
        boardgame_dicts = [boardgame.to_dict() for boardgame in boardgames]
        return JsonResponse(boardgame_dicts, safe=False)
    else:
        raise MethodNotAllowed(request.method, detail=None, code=None)

@api_view(["POST"])
@parser_classes((JSONParser, ))
@csrf_exempt
def share(request: Request, wishlist_id):
    if request.method == 'POST':
        headers = request.META
        token = headers.get("HTTP_TOKEN")
        if token is None:
            raise NotAuthenticated(detail="Token must be provided")
        permission_level = request.data.get("permission_level")
        if not permission_level:
            raise ParseError(detail="Permission level input required.")
        username = request.data.get("username")
        if not username:
            raise ParseError(detail="Username input required.")

        try:
            user = Token.validate_token(token)
        except ValueError:
            raise NotAuthenticated(detail="Token is invalid.")
        try:
            share_status = ShareStatus.get_share_status(permission_level)
        except ValueError:
            raise ParseError(detail="Bad input for permission level.")
        try:
            sharee_user = User.get_user_by_username(username)
        except ValueError:
            raise NotFound(detail=f"No user found with username. (input username={username})")
        wishlist: Wishlist = user.get_editable_wishlist_by_id(wishlist_id)
        if not wishlist:
            raise NotFound(detail=f"Wishlist not found with ID. (wishlist id={wishlist_id})")
        try:
            WishlistSharedUsers.share_wishlist_with_user(wishlist=wishlist, sharee_user=sharee_user, share_status=share_status)
        except ValueError:
            raise ParseError(detail="User already on share list.")

        return Response(status=status.HTTP_204_NO_CONTENT)
    else:
        raise MethodNotAllowed(request.method, detail=None, code=None)

@api_view(["POST", "DELETE"])
@parser_classes((JSONParser, ))
@csrf_exempt
def wishlist_move_boardgame(request: Request, wishlist_id, boardgame_id):
    if request.method == 'POST':
        headers = request.META
        token = headers.get("HTTP_TOKEN")
        if token is None:
            raise NotAuthenticated(detail="Token must be provided.")
        try:
            user = Token.validate_token(token)
        except ValueError:
            raise NotAuthenticated(detail="Token is invalid.")

        wishlist: Wishlist = user.get_editable_wishlist_by_id(wishlist_id)
        if not wishlist:
            raise NotFound(detail=f"Wishlist not found with ID. (wishlist id={wishlist_id})")
        try:
            boardgame = BoardGame.get_boardgame(boardgame_id)
        except ValueError:
            raise NotFound(detail="Board is invalid.")
        WishlistBoardGame.add_boardgame_to_wishlist(wishlist = wishlist, board_game = boardgame)

        return Response(status=status.HTTP_201_CREATED)
    
    elif request.method == 'DELETE':
        headers = request.META
        token = headers.get("HTTP_TOKEN")
        if token is None:
            raise NotAuthenticated(detail="Token must be provided.")
        try:
            user = Token.validate_token(token)
        except ValueError:
            raise NotAuthenticated(detail="Token is invalid.")
        wishlist: Wishlist = user.get_editable_wishlist_by_id(wishlist_id)
        if not wishlist:
            raise NotFound(detail=f"Wishlist not found with ID. (wishlist id={wishlist_id})")
        try:
            boardgame = BoardGame.get_boardgame(boardgame_id)
        except ValueError:
            raise NotFound(detail="Board is invalid.")
        WishlistBoardGame.remove_boardgame_from_wishlist(wishlist = wishlist, board_game = boardgame)

        return Response(status=status.HTTP_204_NO_CONTENT)
    else:
        raise MethodNotAllowed(request.method, detail=None, code=None)

@api_view(["DELETE"])
@parser_classes((JSONParser, ))
@csrf_exempt
def remove_user(request: Request, wishlist_id, user_id):
    if request.method == 'DELETE':
        headers = request.META
        token = headers.get("HTTP_TOKEN")
        if token is None:
            raise NotAuthenticated(detail="Token must be provided")

        try:
            user = Token.validate_token(token)
        except ValueError:
            raise NotAuthenticated(detail="Token is invalid.")
        try:
            remove_user = User.get_user_by_id(user_id=user_id)
        except ValueError:
            raise NotFound(detail=f"No user found with id. (input username={username})")
        wishlist: Wishlist = user.get_editable_wishlist_by_id(wishlist_id)
        if not wishlist:
            raise NotFound(detail=f"Wishlist not found with ID. (wishlist id={wishlist_id})")
        try:
            WishlistSharedUsers.unshare_wishlist_with_user(wishlist=wishlist, remove_user=remove_user)
        except ValueError:
            raise ParseError(detail="User not on share list.")

        return Response(status=status.HTTP_204_NO_CONTENT)
    else:
        raise MethodNotAllowed(request.method, detail=None, code=None)

@api_view(["GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
def user(request: Request):
    if request.method == 'GET':
        headers = request.META
        token = headers.get("HTTP_TOKEN")
        if token is None:
            raise NotAuthenticated(detail="Token must be provided.")
        try:
            user = Token.validate_token(token)
        except ValueError:
            raise NotAuthenticated(detail="Token is invalid.")
        user_info = user.to_dict()
        return JsonResponse(user_info, safe=False)
    else:
        raise MethodNotAllowed(request.method, detail=None, code=None)

@api_view(["GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
def top_five_boardgames(request: Request):
    if request.method == 'GET':
        try:
            with connection.cursor() as cursor:
                cursor.execute(
                    """select count (*) as wishlist_count, board_game_id 
                    from (SELECT distinct board_game_id, user_id
                    FROM public."BoardGameWishlistApp_wishlistboardgame"
                    join public."BoardGameWishlistApp_wishlist" on public."BoardGameWishlistApp_wishlistboardgame".wishlist_id=public."BoardGameWishlistApp_wishlist".id) as boardgameuser
                    group by board_game_id
                    order by wishlist_count desc
                    limit 5;"""
                )
                rows = cursor.fetchall()
        except Exception as e:
            print(e)
            raise e
        topboardgames = []
        for row in rows:
            topboardgames.append(
                BoardGame.get_boardgame(row[1]).to_dict()
            )
        return JsonResponse(topboardgames, safe=False)
    else:
        raise MethodNotAllowed(request.method, detail=None, code=None)


@api_view(["POST", "GET"])
@parser_classes((JSONParser, ))
@csrf_exempt
def public_wishlist(request: Request):
    if request.method == 'GET':
        wishlists = Wishlist.get_public_wishlists()
        wishlists_dicts = []
        for wishlist in wishlists:
            wishlists_dicts.append(wishlist.to_dict(override_user_status="READ"))

        return JsonResponse(wishlists_dicts, safe=False)
    else:
        raise MethodNotAllowed(request.method, detail=None, code=None)

