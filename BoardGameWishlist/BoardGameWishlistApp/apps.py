from django.apps import AppConfig


class BoardgamewishlistappConfig(AppConfig):
    name = 'BoardGameWishlistApp'
