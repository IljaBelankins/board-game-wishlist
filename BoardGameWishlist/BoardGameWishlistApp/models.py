import uuid
from django.db import models
from datetime import datetime, timedelta
from django.contrib.auth.hashers import make_password, check_password
from django.db.utils import IntegrityError

def expire_in_one_hour():
    return datetime.now() + timedelta(hours=1)

class User(models.Model):
    username = models.CharField(max_length=50)
    email = models.EmailField(unique=True)
    created_at = models.DateField(auto_now_add=True)
    password = models.CharField(max_length=1000)

    def __str__(self):
        return self.username
    
    @staticmethod
    def create_user(username: str, email: str, password:str):
        user = User(username=username, email=email, password=make_password(password))
        try:
            user.save()
        except IntegrityError as e:
            print (f'Caught exception while saving User: {str(e)}')
            raise ValueError("Duplicate column value.")

        return user

    def update_password(self, password:str):
        self.password = make_password(password)
        self.save()

    @staticmethod
    def login(username: str, password:str):
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            raise ValueError("Invalid username.")
        if not check_password(password, user.password):
            raise ValueError("Invalid password.")
        return user

    def get_viewable_lists(self):
        wishlists = []
        wishlists.extend(
            self.wishlist_set.all()
        )
        shared_lists = self.wishlistsharedusers_set.all()
        for shared_list in shared_lists:
            wishlists.append(shared_list.wishlist)
        return wishlists

    def get_wishlist_by_id(self, wishlist_id: int):
        all_wishlists = self.get_viewable_lists()
        for wishlist in all_wishlists:
            if wishlist.pk == wishlist_id:
                return wishlist
        return None

    def get_editable_wishlists(self):
        wishlists = []
        wishlists.extend(
            self.wishlist_set.all()
        )
        shared_lists = self.wishlistsharedusers_set.all()
        for shared_list in shared_lists:
            if shared_list.share_status.status == "WRITE":
                wishlists.append(shared_list.wishlist)
        return wishlists

    def get_editable_wishlist_by_id(self, wishlist_id: int):
        all_wishlists = self.get_editable_wishlists()
        for wishlist in all_wishlists:
            if wishlist.pk == wishlist_id:
                return wishlist
        return None

    def get_owned_wishlist_by_id(self, wishlist_id: int):
        try:
            wishlist = self.wishlist_set.get(id=wishlist_id)
            return wishlist
        except Wishlist.DoesNotExist:
            return None

    @staticmethod
    def get_user_by_username(username: str):
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            raise ValueError("Invalid username.")
        return user

    @staticmethod
    def get_user_by_username_and_email(username: str, email: str):
        try:
            user = User.objects.get(username=username, email=email)
        except User.DoesNotExist:
            raise ValueError("Invalid username.")
        return user

    @staticmethod
    def get_user_by_id(user_id: int):
        try:
            user = User.objects.get(id=user_id)
        except User.DoesNotExist:
            raise ValueError("Invalid user id.")
        return user

    def to_dict(self):
        return {
            "id": self.pk,
            "username": self.username,
            "email": self.email,
            "created_at": self.created_at
            }
  

class BoardGame(models.Model):
    name = models.CharField(max_length=1000, db_index=True)
    min_players = models.IntegerField(blank=True, null=True)
    max_players = models.IntegerField(blank=True, null=True)
    publisher = models.CharField(max_length=1000, blank=True, null=True)
    description = models.CharField(max_length=20000, blank=True, null=True)
    age = models.IntegerField(blank=True, null=True)
    image = models.CharField(max_length=10000, blank=True, null=True)

    def __str__(self):
        return self.name

    def to_dict(self):
        output = {
            "id": self.pk,
            "name": self.name,
            "min-players": self.min_players,
            "max-players": self.max_players,
            "publisher": self.publisher,
            "description": self.description,
            "age": self.age,
            "image": self.image
        }
        return output
    
    @staticmethod
    def get_boardgame(boardgame_id: int):
        try:
            boardgame = BoardGame.objects.get(id=boardgame_id)
        except BoardGame.DoesNotExist:
            raise ValueError("Invalid boardgame ID.")
        return boardgame

    @staticmethod
    def create_boardgame(
            boardgame_id:int, 
            name:str, 
            min_age:int, 
            min_players:int, 
            max_players:int, 
            description:str, 
            publisher:str, 
            image:str
        ):
        boardgame = BoardGame(
                id=boardgame_id, 
                name=name, 
                min_players=min_players, 
                max_players=max_players,
                publisher=publisher,
                description=description,
                age=min_age,
                image=image
            )
        boardgame.save()
        return boardgame


class Token (models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateField(auto_now_add=True)
    expires_at = models.DateField(default=expire_in_one_hour)

    @staticmethod
    def create_token(user: User):
        token = Token()
        token.user = user
        token.save()
        return token

    @staticmethod
    def validate_token(token: str) -> User:
        try:
            token = Token.objects.get(id=token)
        except Token.DoesNotExist:
            raise ValueError("Invalid token.")
        if token.expires_at < datetime.now().date():
            raise ValueError("Expired token.")
        return token.user


    

class EmailResetToken(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateField(auto_now_add=True)
    expires_at = models.DateField(default=expire_in_one_hour)

    @staticmethod
    def create_email_reset_token(user):
        reset_token = EmailResetToken()
        reset_token.user = user
        reset_token.save()
        return reset_token

    @staticmethod
    def get_user_from_token(token_id):
        try:
            email_reset_token = EmailResetToken.objects.get(id = token_id)
        except EmailResetToken.DoesNotExist:
            raise ValueError("Invalid token.")
        if email_reset_token.expires_at < datetime.now().date():
            raise ValueError("Expired token.")
        return email_reset_token.user

    @staticmethod
    def delete_token_by_id(token_id):
        try:
            email_reset_token = EmailResetToken.objects.get(id = token_id)
        except EmailResetToken.DoesNotExist:
            raise ValueError("Invalid token.")
        email_reset_token.delete()        




class VisibilityStatus(models.Model):
    status = models.CharField(max_length=100)

    @staticmethod
    def get_status(status: str):
        try:
            status = VisibilityStatus.objects.get(status=status)
        except VisibilityStatus.DoesNotExist:
            raise ValueError("Invalid status.")
        return status


class ShareStatus(models.Model):
    status = models.CharField(max_length=100)

    @staticmethod
    def get_share_status(status: str):
        try:
            share_status = ShareStatus.objects.get(status=status)
        except ShareStatus.DoesNotExist:
            raise ValueError("Invalid share status.")
        return share_status

class Wishlist(models.Model):
    name = models.CharField(max_length=80)
    visibility_status = models.ForeignKey(VisibilityStatus, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.name

    @staticmethod
    def create_wishlist(user: User, name: str, visibility_status: str):
        wishlist = Wishlist(name=name) 
        wishlist.user = user
        wishlist.visibility_status = VisibilityStatus.get_status(visibility_status)
        wishlist.save()
        return wishlist

    def update_wishlist(self, name:str, visibility_status:str):
        self.name = name
        self.visibility_status = VisibilityStatus.get_status(visibility_status)
        self.save()

    @staticmethod
    def get_public_wishlists():
        public_wishlists = Wishlist.objects.filter(visibility_status__status = "PUBLIC")
        return public_wishlists

    def to_dict(self, user_id=None, override_user_status=None):
        user_status = "OWNER"
        wishlist_board_games = self.wishlistboardgame_set.all()
        board_game_dicts = []
        for wishlist_board_game in wishlist_board_games:
            board_game = wishlist_board_game.board_game
            board_game_dicts.append(board_game.to_dict())
        shared_users = self.wishlistsharedusers_set.all()
        read_users = []
        write_users = []
        for shared_user in shared_users:
            if shared_user.share_status.status == "READ":
                if shared_user.user.pk is user_id:
                    user_status="READ"
                read_users.append(shared_user.to_dict())
            elif shared_user.share_status.status == "WRITE":
                if shared_user.user.pk is user_id:
                    user_status="WRITE"
                write_users.append(shared_user.to_dict())
            else:
                print ("Shared user has invalid status.")
        if override_user_status:
            user_status = override_user_status
        output = {
            "name": self.name,
            "visibility": self.visibility_status.status,
            "created_at": str(self.created_at),
            "id": self.pk,
            "board_games": board_game_dicts,
            "read_users": read_users,
            "write_users": write_users,
            "user_id": self.user.pk,
            "user_status": user_status
        }
        return output

    @staticmethod
    def get_public_wishlist(wishlist_id:int):
        try:
            wishlist = Wishlist.objects.get(id=wishlist_id)
        except Wishlist.DoesNotExist:
            return None
        if wishlist.visibility_status.status == "PUBLIC":
            return wishlist
        else:
            return None

        


class WishlistSharedUsers(models.Model):
    wishlist =  models.ForeignKey(Wishlist, on_delete=models.CASCADE)
    user =  models.ForeignKey(User, on_delete=models.CASCADE)
    share_status =  models.ForeignKey(ShareStatus, on_delete=models.CASCADE)

    def to_dict(self):
        return {
            "id": self.user.pk,
            "username": self.user.username,
            "email": self.user.email,
            "created_at": self.user.created_at
            }
            
    @staticmethod
    def share_wishlist_with_user(wishlist, sharee_user, share_status):
        try:
            WishlistSharedUsers.objects.get(wishlist = wishlist, user = sharee_user)
            raise ValueError("User already on share list.")
        except WishlistSharedUsers.DoesNotExist:
            pass     
        wishlistshareduser = WishlistSharedUsers()
        wishlistshareduser.user = sharee_user
        wishlistshareduser.share_status = share_status
        wishlistshareduser.wishlist = wishlist
        wishlistshareduser.save()

    @staticmethod
    def unshare_wishlist_with_user(wishlist, remove_user):
        try:
            wishlistshareduser = WishlistSharedUsers.objects.get(wishlist = wishlist, user = remove_user)
        except WishlistSharedUsers.DoesNotExist:
            raise ValueError("Invalid ID(s).")
        wishlistshareduser.delete()       

        






class WishlistBoardGame(models.Model):
    wishlist = models.ForeignKey(Wishlist, on_delete=models.CASCADE)
    board_game = models.ForeignKey(BoardGame, on_delete=models.CASCADE)

    @staticmethod
    def add_boardgame_to_wishlist(wishlist: Wishlist, board_game: BoardGame):
        try:
            wishlistboardgame = WishlistBoardGame.objects.get(wishlist = wishlist, board_game = board_game)
            raise ValueError("Boardgame already inside wishlist.")
        except WishlistBoardGame.DoesNotExist:
            pass     
        wishlistboardgame = WishlistBoardGame()
        wishlistboardgame.wishlist = wishlist
        wishlistboardgame.board_game = board_game
        wishlistboardgame.save()
        
    @staticmethod
    def remove_boardgame_from_wishlist(wishlist: Wishlist, board_game: BoardGame):
        try:
            wishlistboardgame = WishlistBoardGame.objects.get(wishlist = wishlist, board_game = board_game)
        except WishlistBoardGame.DoesNotExist:
            raise ValueError("Invalid ID(s).")
        wishlistboardgame.delete()       

