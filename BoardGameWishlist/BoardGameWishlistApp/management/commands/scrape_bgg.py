from django.core.management.base import BaseCommand, CommandError
import requests
from xml.etree import ElementTree
from BoardGameWishlistApp.models import BoardGame
from typing import Dict, List
from time import sleep
#TODO THIS ERRORS ON A PROJECT WITH A RIDICILOUSLY LONG DESCRIPTION. FIX DESCRIPTION SIZE!
class Command(BaseCommand):
    help = 'Scrapes BGG for board games.'

    def handle(self, *args, **options):
        for i in range(0, 7000, 500):
            id_range = range(i, i+500)
            converted_id_range = [str(i) for i in id_range]
            boardgame_ids = ",".join(converted_id_range)
            self.stdout.write(self.style.SUCCESS(f"Range {i} - {i+500}"))
            body = self.scrape_boardgame_by_id(boardgame_ids)


            boardgames = self.extract_boardgame_data_from_xml_string(body)      
            for boardgame in boardgames:
                BoardGame.create_boardgame(
                        boardgame["boardgame_id"], 
                        boardgame["name"], 
                        boardgame["min_age"], 
                        boardgame["min_players"], 
                        boardgame["max_players"], 
                        boardgame["description"], 
                        boardgame["publisher"], 
                        boardgame["image"]
                    )

        self.stdout.write(self.style.SUCCESS('Successfully'))

    def scrape_boardgame_by_id(self, boardgame_ids:str) -> str:
        result = False
        while not result:
            url = f'https://www.boardgamegeek.com/xmlapi2/thing?id={boardgame_ids}&type=boardgame'
            headers = {}
            r = requests.get(url, headers=headers)
            body = r.text
            if "Rate limit exceeded." in body:
                self.stdout.write(self.style.SUCCESS("Waiting for rate limit."))
                sleep(20)
                continue
            return body

    def extract_boardgame_data_from_xml_string(self, xml:str) -> List[Dict]:
        boardgames = []
        root = ElementTree.fromstring(xml)
        for result in root:
            boardgame = self.extract_single_boardgame_from_elementtree(result)
            if boardgame is not None:
                boardgames.append(boardgame)
        
        return boardgames
        

    def extract_single_boardgame_from_elementtree(self, result) -> Dict:
        boardgame = {}
        boardgame_id = result.attrib["id"]
        self.stdout.write(self.style.SUCCESS(boardgame_id))
        image = None
        try:
            image = result.find("image").text
        except:
            pass
        min_players = result.find("minplayers").attrib["value"] if result.find("minplayers").attrib["value"].isdigit() else None
        max_players = result.find("maxplayers").attrib["value"] if result.find("maxplayers").attrib["value"].isdigit() else None
        description = result.find("description").text[:15000] if result.find("description").text else None
        min_age = result.find("minage").attrib["value"] if result.find("minage").attrib["value"].isdigit() else None
        name = result.findall("name")[0].attrib["value"]   
        links = result.findall("link")
        publisher = None
        for link in links:
            if ('type', 'boardgamepublisher') in link.attrib.items():
                publisher = link.attrib["value"]
                break
        

        boardgame["boardgame_id"]=boardgame_id
        boardgame["name"]=name
        boardgame["min_age"]=min_age
        boardgame["min_players"]=min_players
        boardgame["max_players"]=max_players
        boardgame["description"]=description
        boardgame["publisher"]=publisher
        boardgame["image"]=image
        return boardgame

