'''
Features:
    -Username, password and email sign up + login.
    -Password/username recovery.
    -Search Board Game Geek using their API.
    -Create a wishlist/multiple wishlists using API.
        -Wishlists can be made private, public or shared with other user(s)
    

Plan:

1) Create database for the storage of username, password and email.
    -Like for the group project, the database should have a salt and hash for passwords
    -When a user inputs a username and login, the website should check

2)Create sign up form.
    -HTML site that will check to see if an email has a valid format, check to see
    if the username is unique or taken (Optional: Add suggestions for valid usernames
    such as IljaBelankins12334), and check to see if password uses characters we 
    do not want them using (Optional: create password rules such as minimum length,
    uppercase, lowercase and so on).

3)Create sign-in form.
    -Check the user table to see if the two match. If correct, login is granted.
    (Optional: Tell the user if the username they have entered already exists or not on 
    failed attempt.)

4) Create password/username recovery form.
    -The user may use either their username or password for recovery.
        -Recovering the username will send the username to the email.
        -Recovering the password will send a special page for 
        entering a new password with confirmation.

5) Scrape the BGG API and store the information in a database.
    -We will check the format of the data and adapt the database accordingly.

6) It seems the DBB API has many different kinds of searches you can do. We will try implementing as many
as we can (ideally all of them).

7) Design a way to create wishlists out of the items in our database.
    -My idea is to use a system similar to how YouTube creates their playlists, or Amazon.
    These are the ways you will be able to make wishlists:
        -Go to a page where a Board Game is displayed and then add it to a wishlist OR 
        create a new wishlist on the spot and add it in immediately. This new wishlist
        is now usable on other items.
        -Go to a special wishlist tab where you are able to generate an empty wishlist, edit
        existing wishlists or delete wishlists all together.



'''