from django.test import TestCase, Client
from BoardGameWishlistApp.models import Wishlist, User, WishlistSharedUsers, ShareStatus, Token
from django.urls import reverse
import json

class WishlistViewTestCase(TestCase):
    def setup(self):
        self.owner_user = User.create_user(username="TestA", email="test@test.com", password="TestA")
        self.share_user = User.create_user(username="TestB", email="testB@test.com", password="TestB")
        self.write_user = User.create_user(username="TestC", email="testC@test.com", password="TestC")
        self.random_user = User.create_user(username="TestD", email="testD@test.com", password="TestD")
        self.wishlist = Wishlist.create_wishlist(name="Test Wishlist", user=self.owner_user, visibility_status="PUBLIC")
        share_status = ShareStatus.get_share_status("READ")
        WishlistSharedUsers.share_wishlist_with_user(self.wishlist, self.share_user, share_status)
        share_status = ShareStatus.get_share_status("WRITE")
        WishlistSharedUsers.share_wishlist_with_user(self.wishlist, self.write_user, share_status)

    def test_view_with_read_user(self):
        self.setup()
        client = Client()
        token = Token.create_token(self.share_user)
        url = reverse("wishlist_by_id", kwargs={"wishlist_id": self.wishlist.pk})
        header={"HTTP_TOKEN": token.id}
        response = client.get(url, **header)
        assert True is True

    def test_get_public_wishlist(self):
        self.setup()
        client = Client()
        token = Token.create_token(self.random_user)
        url = reverse("wishlist_by_id", kwargs={"wishlist_id": self.wishlist.pk})
        header={"HTTP_TOKEN": token.id}
        response = client.get(url, **header)
        data = json.loads(response.content)
        assert data["user_status"]=="READ"
