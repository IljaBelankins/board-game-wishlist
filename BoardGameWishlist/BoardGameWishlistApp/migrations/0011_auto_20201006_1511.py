# Generated by Django 3.1.1 on 2020-10-06 14:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('BoardGameWishlistApp', '0010_auto_20201006_1508'),
    ]

    operations = [
        migrations.AlterField(
            model_name='boardgame',
            name='description',
            field=models.CharField(blank=True, max_length=20000, null=True),
        ),
    ]
